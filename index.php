<?php include("db.php"); ?>

<?php include('includes/header.php'); ?>

<main class="container p-4">
  <div class="row">
    <div class="col-md-4">


      <?php if (isset($_SESSION['message'])) { ?>
      <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
        <?= $_SESSION['message']?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php session_unset(); } ?>


      <div class="card card-body">
        <form action="save_task.php" method="POST">
          <div class="form-group">
            <input type="text" name="nombre" class="form-control" placeholder="Nombre Auto" autofocus>
          </div>
          <div class="form-group">
            <textarea name="descripcion" rows="2" class="form-control" placeholder="Descripcion"></textarea>
          </div>
          <div class="form-group">
            <input type="text" name="tipoCombustible" class="form-control" placeholder="Tipo Combistible" autofocus>
          </div>
          <div class="form-group">
            <input type="number" name="cantidadPuertas" class="form-control" placeholder="Cantidad de puertas" autofocus>
          </div>
          <div class="form-group">
            <input type="number" name="precio" class="form-control" placeholder="Precio" autofocus>
          </div>
          <input type="submit" name="guardar" class="btn btn-success btn-block" value="Guardar">
        </form>
      </div>


    </div>
    <div class="col-md-8">
      <table class="table table-bordered" id="table_id">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Descripcion</th>
            <th>Tipo Combustible</th>
            <th>Precio</th>
            <th>Action</th>
          </tr>
        </thead>
        
      </table>
    </div>
  </div>
</main>

<?php include('includes/footer.php'); ?>
